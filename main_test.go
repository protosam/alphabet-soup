package main

import (
	"bytes"
	"strings"
	"testing"
)

var dataThatMustPass = []map[string]string{
	{
		"data": `5x5
H A S D F
G E Y B H
J K L Z X
C V B L N
G O O D O
HELLO
GOOD
BYE`,
		"results": `HELLO 0:0 4:4
GOOD 4:0 4:3
BYE 1:3 1:1`,
	},
	{
		"data": `5x5
H A S D F
G E Y B H
J K L Z X
C V B L N
G O O D O
HELLO
GOOD
BYE
`,
		"results": `HELLO 0:0 4:4
GOOD 4:0 4:3
BYE 1:3 1:1`,
	},
	{
		"data": `3x3
A B C
D E F
G H I
ABC
AEI`,
		"results": `ABC 0:0 0:2
AEI 0:0 2:2`,
	},
	{
		"data": `3x3
A B C
D E F
G H I
AB C
AEI`,
		"results": `AB C 0:0 0:2
AEI 0:0 2:2`,
	},
}

func TestFindWords(t *testing.T) {
	for i := range dataThatMustPass {
		wl, err := FindWords(bytes.NewBufferString(dataThatMustPass[i]["data"]))
		if err != nil {
			t.Fatalf("ERROR: on words that must pass (%d): %s", i, err)
		}

		results := strings.Split(dataThatMustPass[i]["results"], "\n")

		if len(results) != len(wl) {
			t.Fatalf("ERROR: expected %d results but got %d (%d)", len(wl), len(results), i)
		}

		for x, result := range results {
			if wl[x].String() != result {
				t.Fatalf("ERROR: result mismatch: '%s' != '%s' (%d)", wl[x].String(), result, i)
			}
		}
	}

	// TODO: should probably make some tests for failures too, I've just
	//       decided that failure is not an option here though. jk
	//       the instructions don't really call for any testing at all
}
