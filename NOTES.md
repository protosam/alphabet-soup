There are some test files contained in `testdata/`.

Here are a couple of examples on how to run this solution:
```
cat testdata/test1.txt | go run main.go -file -

go run main.go -file testdata/test1.txt
```

A gotest can be performed by running `go test` to validate the solution.