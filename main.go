package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

// WordLocation describes where a word is in a word search puzzle
type WordLocation struct {
	Word        string
	StartColumn int
	StartRow    int
	EndColumn   int
	EndRow      int
}

func (w *WordLocation) String() string {
	return fmt.Sprintf("%s %d:%d %d:%d", w.Word, w.StartRow, w.StartColumn, w.EndRow, w.EndColumn)
}

// FindWords reads word search data and returns the locations of all the words
func FindWords(wordSearch io.Reader) (WordLocations []*WordLocation, _ error) {
	// scanner is used to seek out details from the word search data
	// by default it will do line scans, which works for parsing the word search
	scanner := bufio.NewScanner(wordSearch)

	// Step 1 - Identify the board size provided in the first line
	if !scanner.Scan() {
		if scanErr := scanner.Err(); scanErr != nil {
			return nil, fmt.Errorf("ERROR: failed to scan first line: %s", scanErr)
		}

		return nil, fmt.Errorf("ERROR: failed to scan first line - was the file provided empty?")
	}

	// prepare the board size data to be converted to integers describing the
	// number rows and columns the board contains
	boardSize := strings.SplitN(string(scanner.Bytes()), "x", 2)

	// parse the row size or throw an error
	rows, err := strconv.Atoi(boardSize[0])
	if err != nil {
		return nil, fmt.Errorf("ERROR: failed read row count: %s", err)
	}

	// parse the column size or throw an error
	cols, err := strconv.Atoi(boardSize[1])
	if err != nil {
		return nil, fmt.Errorf("ERROR: failed read column count: %s", err)
	}

	// Step 2 - Read in the board data and build the board
	var board [][]byte

	// read each line in for each row
	for i := 0; i < rows; i++ {
		if !scanner.Scan() {
			if scanErr := scanner.Err(); scanErr != nil {
				return nil, fmt.Errorf("ERROR: failed to scan row %d: %s", i, scanErr)
			}

			return nil, fmt.Errorf("ERROR: failed to scan row %d: reason unknown", i)
		}

		// build the row from the unprocessed rowData
		row := []byte{}
		rowData := scanner.Bytes()
		for _, b := range rowData {
			if b != ' ' {
				row = append(row, b)
			}
		}

		// fail whenever garbage is provided
		if len(row) != cols {
			return nil, fmt.Errorf("ERROR: failed to scan row %d: expected %d columns but got %d: '%s'", i, cols, len(row), row)
		}

		// add row to the board
		board = append(board, row)
	}

	// Phase 3 - Discover words on the board
	for scanner.Scan() {
		word := scanner.Bytes()

		// handle errors
		if scanErr := scanner.Err(); scanErr != nil && scanErr != io.EOF {
			return nil, fmt.Errorf("ERROR: failed to scan word '%s': %s", word, scanErr)
		}

		// break on empty newlines because piped input often ends this way
		if len(word) == 0 {
			break
		}

		// Find the word
		wl, found := FindWord(board, word)

		// provide word not found error if it wasn't found
		if !found {
			return nil, fmt.Errorf("ERROR: word not found on board '%s'", word)
		}

		WordLocations = append(WordLocations, wl)
	}

	return WordLocations, nil
}

// Used to maintain direction when seeking words on a board. This value never
// changes and should be a const, but go doesn't support const slices.
var cardinalDirections = [8][2]int{
	// These values can be thought of as {row, col}
	{-1, 0},  // up
	{1, 0},   // down
	{0, 1},   // right
	{0, -1},  // left
	{1, 1},   // diagnal - down + right
	{1, -1},  // diagnal - down + left
	{-1, 1},  // diagnal - up + right
	{-1, -1}, // diagnal - up + left
}

// FindWord will search a 2D array of bytes and provide location data describing
// where the word is.
// The search algorithm performs the following steps.
// - Find positions of first letter matches on the board
// - Test each cardinal direction for the last letter relative to the first letter
// - Validate letters between first letter and last letter for full match
func FindWord(board [][]byte, word []byte) (_ *WordLocation, found bool) {
	// formatted word for testing
	testWord := []byte(strings.ReplaceAll(string(word), " ", ""))

	// get the word length here to avoid counting it a bunch of times later
	wordLen := len(testWord)
	maxCol := len(board[0])
	maxRow := len(board)

	// iterate over all the letters on the board for matches
	for startRow, row := range board {
		for startCol, letter := range row {
			// test each letter to determine for match on first letter of the word
			if letter == testWord[0] {
				// test each last letter in each cardinalDirection
				for _, heading := range cardinalDirections {
					// identify location in this direction where last letter would be
					lastRow := startRow + (heading[0] * (wordLen - 1))
					lastCol := startCol + (heading[1] * (wordLen - 1))
					if lastRow >= 0 && lastRow < maxRow &&
						lastCol >= 0 && lastCol < maxCol &&
						testWord[wordLen-1] == board[lastRow][lastCol] {

						// test letters between first and last positions
						success := true
						for i := 1; i < wordLen-1; i++ {
							currentRow := startRow + (heading[0] * i)
							currentCol := startCol + (heading[1] * i)

							// flag failure and escape in-between testing upon
							// finding a letter that does not match the word
							if board[currentRow][currentCol] != testWord[i] {
								success = false
								break
							}
						}

						// the word was found, return the data
						if success {
							return &WordLocation{
								Word:        string(word),
								StartRow:    startRow,
								StartColumn: startCol,
								EndRow:      lastRow,
								EndColumn:   lastCol,
							}, true
						}
					}
				}
			}
		}
	}

	return nil, false
}

func main() {
	// define cmd flags
	filePath := flag.String("file", "", "path of file to read")

	// parse flags
	flag.Parse()

	// open the file
	var file *os.File
	var err error

	if *filePath == "-" || *filePath == "/dev/stdin" {
		file = os.Stdin
	} else {
		file, err = os.Open(*filePath)
		if err != nil {
			log.Fatalf("%s", err)
		}
	}

	// find word locations
	wordList, err := FindWords(file)
	if err != nil {
		log.Fatalf("%s", err)
	}

	// output locations
	for i := range wordList {
		fmt.Println(wordList[i])
	}
}
